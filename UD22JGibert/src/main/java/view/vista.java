package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controler.ClientControler;
import dto.Cliente;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class vista extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textNombre;
	private JTextField textApellido;
	private JTextField textDNI;
	private JTextField textDireccion;
	private ClientControler clientControler; //objeto personaController que permite la relacion entre esta clase y la clase PersonaController

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public void setCoordinador(ClientControler clientControler) {
		this.clientControler=clientControler;
	} 
	
	public vista() {
		setTitle("Añadir cliente");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 277, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ID:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(78, 18, 86, 14);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(78, 43, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre:");
		lblNewLabel_1.setBounds(10, 74, 65, 14);
		contentPane.add(lblNewLabel_1);
		
		textNombre = new JTextField();
		textNombre.setBounds(10, 99, 86, 20);
		contentPane.add(textNombre);
		textNombre.setColumns(10);
		
		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setBounds(10, 130, 65, 14);
		contentPane.add(lblApellido);
		
		textApellido = new JTextField();
		textApellido.setColumns(10);
		textApellido.setBounds(10, 155, 86, 20);
		contentPane.add(textApellido);
		
		JLabel lblNewLabel_1_1 = new JLabel("DNI:");
		lblNewLabel_1_1.setBounds(160, 74, 46, 14);
		contentPane.add(lblNewLabel_1_1);
		
		textDNI = new JTextField();
		textDNI.setColumns(10);
		textDNI.setBounds(160, 99, 86, 20);
		contentPane.add(textDNI);
		
		JLabel lblDirecion = new JLabel("Direcion:");
		lblDirecion.setBounds(160, 130, 65, 14);
		contentPane.add(lblDirecion);
		
		textDireccion = new JTextField();
		textDireccion.setColumns(10);
		textDireccion.setBounds(160, 155, 86, 20);
		contentPane.add(textDireccion);
		
		JButton btn = new JButton("Enviar datos");
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				try {
					int id=Integer.parseInt(textField.getText());
					int DNI=Integer.parseInt(textDNI.getText());
					String nombre=textNombre.getText();
					String direccion=textDireccion.getText();
					String apellido=textApellido.getText();
					
					Cliente cliente = new Cliente(id,DNI,nombre,apellido,direccion);
					
					clientControler.registrarCliente(cliente);	
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null,"Error en el Ingreso de Datos","Error",JOptionPane.ERROR_MESSAGE);
					System.out.println(ex);
				}
			}
		});
		btn.setBounds(78, 212, 104, 23);
		contentPane.add(btn);
	}
}
