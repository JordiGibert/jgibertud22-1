package controler;



import dto.Cliente;
import service.ClienteServ;
import view.VentanaBuscar;
import view.VentanaPrincipal;
import view.vista;

public class ClientControler {
	private ClienteServ clienteServ;
	private VentanaBuscar miVentanaBuscar;
	private VentanaPrincipal miVentanaPrincipal;
	private vista vista;
	
	//Metodos getter Setters de vistas


	public vista getvista() {
		return vista;
	}
	public void setvista(vista vista) {
		this.vista = vista;
	}
	
	public ClienteServ getClienteServ() {
		return clienteServ;
	}
	public void setClienteServ(ClienteServ clienteServ) {
		this.clienteServ = clienteServ;
	}
	
	public VentanaBuscar getMiVentanaBuscar() {
		return miVentanaBuscar;
	}
	public void setMiVentanaBuscar(VentanaBuscar miVentanaBuscar) {
		this.miVentanaBuscar = miVentanaBuscar;
	}
	
	public void setMiVentanaPrincipal(VentanaPrincipal miVentanaPrincipal) {
		this.miVentanaPrincipal = miVentanaPrincipal;
	}
	
	//Hace visible las vistas de Registro y Consulta
	public void mostrarvista() {
		vista.setVisible(true);
	}
	public void mostrarVentanaConsulta() {
		miVentanaBuscar.setVisible(true);
	}

	//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
	public void registrarCliente(Cliente miCliente) {
		clienteServ.validarRegistro(miCliente);
	}
	
	public Cliente buscarCliente(String codigoCliente) {
		return clienteServ.validarConsulta(codigoCliente);
	}
	
	public void modificarCliente(Cliente miCliente) {
		clienteServ.validarModificacion(miCliente);
	}
	
	public void eliminarCliente(String codigo) {
		clienteServ.validarEliminacion(codigo);
	}
	
	


}
