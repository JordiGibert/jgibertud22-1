package com.UD22.JGibert.UD22JGibert;



import controler.ClientControler;
import service.ClienteServ;
import view.VentanaBuscar;
import view.VentanaPrincipal;
import view.vista;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	/*Se instancian las clases*/
    	vista vista=new vista();
    	ClienteServ clienteServ=new ClienteServ();
    	VentanaBuscar miVentanaBuscar = new VentanaBuscar();
		ClientControler clientControler=new ClientControler();
		VentanaPrincipal miVentanaPrincipal=new VentanaPrincipal();
		
		/*Se establecen las relaciones entre clases*/
		vista.setCoordinador(clientControler);
		clienteServ.setControler(clientControler);
		miVentanaBuscar.setCoordinador(clientControler);
		miVentanaPrincipal.setCoordinador(clientControler);
		
		/*Se establecen relaciones con la clase coordinador*/
		clientControler.setvista(vista);
		clientControler.setClienteServ(clienteServ);
		clientControler.setMiVentanaBuscar(miVentanaBuscar);
		clientControler.setMiVentanaPrincipal(miVentanaPrincipal);
				
		miVentanaPrincipal.setVisible(true);
    }
}
